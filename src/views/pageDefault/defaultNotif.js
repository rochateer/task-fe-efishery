import React, { Component } from 'react';
import PropTypes from 'prop-types';

const propTypes = {
    actionMessage : PropTypes.string,
    actionStatus : PropTypes.string,
  };

  const defaultProps = {
    actionMessage : null,
    actionStatus : null,
  };


class DefaultNotif extends Component {
    constructor(props) {
        super(props);

        this.state = {
            visible: true,
        };

        this.onDismiss = this.onDismiss.bind(this);
    }

    onDismiss() {
        this.setState({ visible: false });
    }

    render(){
        if(this.props.actionStatus !== undefined && this.props.actionStatus !== null){
            if(this.props.actionStatus === 'failed'){
                return (
                    <div className="row">
                        <div className="col">
                            <div class="alert alert-danger" role="alert">
                                {this.props.actionMessage !== null ? JSON.stringify(this.props.actionMessage) : "Terjadi kesalahan, coba beberapa saat lagi"}
                            </div>
                        </div>
                    </div>
                )
            }else{
                if(this.props.actionStatus === 'success'){
                    return (
                        <div className="row">
                            <div className="col">
                                <div class="alert alert-success" role="alert">
                                    {this.props.actionMessage !== null ? JSON.stringify(this.props.actionMessage) : "Selamat, aksi anda telah berhasil"}
                                </div>
                            </div>
                        </div>
                    )
                }else{
                    if(this.props.actionStatus === 'warning'){
                        return (
                            <div className="row">
                                <div className="col">
                                    <div class="alert alert-warning" role="alert">
                                        {this.props.actionMessage !== null ? JSON.stringify(this.props.actionMessage) : "Terjadi kesalahan, coba beberapa saat lagi"}
                                    </div>
                                </div>
                            </div>
                        )
                    }else{
                        return (
                            <div></div>
                        )
                    }
                }
            }
        }else{
            return (
                <div></div>
            )
        }
    }
}


export default DefaultNotif;
