import React from 'react';
import './scss/style.scss';
import { Route, Switch, BrowserRouter,Redirect } from 'react-router-dom';

const ListPrice = React.lazy(() => import('./views/listPrice/listPrice'));
const ReactLandingPage = React.lazy(() => import('./views/pageDefault/reactLandingPage'));
const CreatePrice = React.lazy(() => import('./views/listPrice/createPrice'));
const EditPrice = React.lazy(() => import('./views/listPrice/editPrice'));

function App() {
  return (
    <BrowserRouter>
      <React.Suspense fallback={"loading.."}>
        <Switch>
          <Route exact path="/" name="React Landing Page" render={props => <ListPrice {...props}/>} />
          <Route exact path="/price-list" name="List Price" render={props => <ListPrice {...props}/>} />
          <Route exact path="/price-create" name="Create Price" render={props => <CreatePrice {...props}/>} />
          <Route exact path="/price-edit/:id" exact name="Create Price" render={props => <EditPrice {...props}/>} />
          <Redirect from="/" to="/price-list" />
        </Switch>
      </React.Suspense>
    </BrowserRouter>
  );
}

export default App;
